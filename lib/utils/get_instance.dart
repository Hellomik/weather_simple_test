import 'package:get_it/get_it.dart';

T getInstance<T extends Object>(T Function() create) {
  if (!GetIt.I.isRegistered<T>()) {
    GetIt.I.registerSingleton<T>(create());
  }
  return GetIt.I.get<T>();
}
