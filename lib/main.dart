import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:weather_test/src/data/model/city/city_dto.dart';
import 'package:weather_test/src/feature/city/blocs/city_bloc.dart';
import 'package:weather_test/src/feature/city/pages/city_page.dart';

Future addCashForTest() async {
  final cityBox = Hive.box<CityDTO>('city');
  await cityBox.clear();
  await Hive.box<CityDTO>('city').addAll(const [
    CityDTO(cityName: "MOSCOW", temperature: 16),
    CityDTO(cityName: "TOKIO", temperature: 48),
    CityDTO(cityName: "New York", temperature: 20),
  ]);
}

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(CityDTOAdapter());
  await Hive.openBox<CityDTO>('city');

  await addCashForTest();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => CityBloc()),
        ],
        child: const CityPage(),
      ),
    );
  }
}
