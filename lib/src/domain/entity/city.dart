import 'package:equatable/equatable.dart';

class City extends Equatable {
  final String cityName;
  final double temperature;

  const City({
    required this.cityName,
    required this.temperature,
  });

  @override
  List<Object?> get props => [cityName, temperature];
}
