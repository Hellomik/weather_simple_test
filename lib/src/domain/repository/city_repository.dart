import 'package:weather_test/src/domain/entity/city.dart';

abstract class CityRepository {
  Future<List<City>> getCurrentCities();
}
