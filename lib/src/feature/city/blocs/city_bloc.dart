import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_test/src/data/repositories/city_repository_impl.dart';
import 'package:weather_test/src/domain/entity/city.dart';
import 'package:weather_test/src/domain/repository/city_repository.dart';
import 'package:weather_test/utils/get_instance.dart';

part 'city_event.dart';
part 'city_state.dart';
part 'reaction/load_city_raection.dart';

class CityBloc extends Bloc<CityEvent, CityState> {
  CityRepository get cityRepository => getInstance(CityRepositoryImpl.new);

  CityBloc() : super(EmptyCityState()) {
    on<LoadCityEvent>(loadCityReaction);
  }
}
