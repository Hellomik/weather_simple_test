part of 'city_bloc.dart';

abstract class CityState extends Equatable {
  const CityState();
}

class EmptyCityState extends CityState {
  @override
  List<Object?> get props => [];
}

class LoadingCityState extends CityState {
  @override
  List<Object?> get props => [];
}

class DataCityState extends CityState {
  final List<City> cities;
  const DataCityState(this.cities);
  @override
  List<Object?> get props => [cities];
}
