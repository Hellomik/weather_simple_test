part of '../city_bloc.dart';

extension LoadCityReaction on CityBloc {
  void loadCityReaction(
    LoadCityEvent event,
    Emitter<CityState> emit,
  ) async {
    try {
      emit(LoadingCityState());
      final city = await cityRepository.getCurrentCities();
      emit(DataCityState(city));
    } catch (e, s) {
      log(e.toString(), stackTrace: s);
    }
  }
}
