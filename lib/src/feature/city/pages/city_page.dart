import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_test/src/feature/city/blocs/city_bloc.dart';
import 'package:weather_test/src/feature/city/pages/temperature_page.dart';

class CityPage extends StatefulWidget {
  const CityPage({super.key});

  @override
  State<CityPage> createState() => _CityPageState();
}

class _CityPageState extends State<CityPage> {
  @override
  void initState() {
    BlocProvider.of<CityBloc>(context, listen: false)
        .add(const LoadCityEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("City Page"),
      ),
      body: BlocBuilder<CityBloc, CityState>(
        builder: (context, state) {
          if (state is DataCityState) {
            return ListView.separated(
              itemBuilder: (context, index) => InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => TemperaturePage(
                        weather: state.cities[index],
                      ),
                    ),
                  );
                },
                child: Container(
                  padding: const EdgeInsets.all(10),
                  child: Text(state.cities[index].cityName),
                ),
              ),
              separatorBuilder: (context, index) => const SizedBox(height: 10),
              itemCount: state.cities.length,
            );
          }
          return const SizedBox();
        },
      ),
    );
  }
}
