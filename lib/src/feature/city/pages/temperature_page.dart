import 'package:flutter/material.dart';
import 'package:weather_test/src/domain/entity/city.dart';

class TemperaturePage extends StatelessWidget {
  final City weather;

  const TemperaturePage({
    required this.weather,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Temperature Page'),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(weather.cityName),
            const SizedBox(height: 10),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Text('Temparute '),
                Text(weather.temperature.toString()),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
