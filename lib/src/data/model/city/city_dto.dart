// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
import 'package:weather_test/src/domain/entity/city.dart';

part 'city_dto.g.dart';

@HiveType(typeId: 0)
class CityDTO extends Equatable {
  @HiveField(0)
  final String cityName;
  @HiveField(1)
  final double temperature;

  const CityDTO({
    required this.cityName,
    required this.temperature,
  });

  @override
  List<Object?> get props => [cityName, temperature];

  City toEntity() {
    return City(
      cityName: cityName,
      temperature: temperature,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'cityName': cityName,
      'temperature': temperature,
    };
  }

  factory CityDTO.fromMap(Map<String, dynamic> map) {
    return CityDTO(
      cityName: (map["cityName"] ?? '') as String,
      temperature: (map["temperature"] ?? 0.0) as double,
    );
  }

  String toJson() => json.encode(toMap());

  factory CityDTO.fromJson(String source) =>
      CityDTO.fromMap(json.decode(source) as Map<String, dynamic>);
}
