// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'city_dto.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CityDTOAdapter extends TypeAdapter<CityDTO> {
  @override
  final int typeId = 0;

  @override
  CityDTO read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CityDTO(
      cityName: fields[0] as String,
      temperature: fields[1] as double,
    );
  }

  @override
  void write(BinaryWriter writer, CityDTO obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.cityName)
      ..writeByte(1)
      ..write(obj.temperature);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CityDTOAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
