import 'dart:developer';

import 'package:weather_test/src/data/datasources/city/city_local_datasource.dart';
import 'package:weather_test/src/data/datasources/city/city_remote_datasource.dart';
import 'package:weather_test/src/domain/entity/city.dart';
import 'package:weather_test/src/domain/repository/city_repository.dart';
import 'package:weather_test/utils/get_instance.dart';

class CityRepositoryImpl implements CityRepository {
  late CityRemoteDataSource remoteDataSource =
      getInstance(CityRemoteDataSource.new);
  late CityLocalDataSource localDataSource =
      getInstance(CityLocalDataSource.new);

  CityRepositoryImpl();

  @override
  Future<List<City>> getCurrentCities() async {
    try {
      final citiesDTO = await remoteDataSource.getCities();

      // Save data to local storage
      localDataSource.saveCities(citiesDTO);

      return citiesDTO.map((e) => e.toEntity()).toList();
    } catch (e, stackTrace) {
      final citiesDTO = localDataSource.getCities();

      // Show Error
      log(e.toString(), stackTrace: stackTrace);

      return citiesDTO.map((e) => e.toEntity()).toList();
    }
  }
}
