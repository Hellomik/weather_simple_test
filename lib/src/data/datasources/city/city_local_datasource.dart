import 'package:hive/hive.dart';
import 'package:weather_test/src/data/model/city/city_dto.dart';

class CityLocalDataSource {
  // can be a DIO or another HTTP client to make request
  // final httpClient;
  Box<CityDTO> get box => Hive.box<CityDTO>('city');

  CityLocalDataSource();

  List<CityDTO> getCities() {
    return Hive.box<CityDTO>('city').values.toList();
  }

  Future<void> saveCities(List<CityDTO> cities) async {
    await box.clear();
    await box.addAll(cities);
  }
}
